<p align="left">
    <img style="display:block;text-align:center" src="./assets/wireapps-logo_inline.svg" alt="logo-text" height="100" />
    <h2>All documents related to the Wireapps projects and products</h2>
</p>


# Main Product
<a href="https://hasithahiran.gitlab.io/wireapps-docs/#/delivergate/">
    <h2>Delivergate</h2>
</a>

<h4>CONTROL ALL YOUR ONLINE ORDERS FROM ONE DEVICE</h4>
<p align="left">
    <img style="display:block;text-align:center" src="./assets/graphics_Food-Delivery-Platforms.png" alt="logo-text" height="400" />
</p>
<p>Connect all delivery platforms to your POS. Print tickets in cashier and kitchen. Manage all online menus at once.</br> Get centralized financial reporting that helps your business grow.</p>

# Projects

