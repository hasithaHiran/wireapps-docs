# Delivergate Front end Document

---


> Development.

<h6 > Install NodeJS from &nbsp; <link>https://nodejs.org/en/download/ </link></h6>


* Download or clone the repository. https://gitlab.com/redlabstech/delivergate-admin-fe.git
* Install dependencies <code>$ npm install </code>
* Start the app <code>$ npm start or $ ng serve</code>
* Build the app <code>$ npm run build or $ ng build </code>

> Folder Structure.

```
| ---  app
    | --- detectives
    | --- helpers
    | --- layouts
    | --- models
    | --- services  

| --- assets
	| --- fonts
    | --- icons
    | --- js
    | --- scss

| --- environments
```


    

